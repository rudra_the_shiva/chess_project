# Chess pieces movement

To write a set of functions to determine possible moves of a given chess piece, on an empty board

Using these to write a function to check if two pieces are attacking each othjer

1. rook\_moves(position)

        Return the list of possible moves from the given position.


2. knight\_moves(position)

        Return the list of possible moves from the given position.


3. queen\_moves(position)

        Return the list of possible moves from the given position.


4. bishop\_moves(position)

        Return the list of possible moves from the given position.

5. check\_attack(piece1, position1, piece2, position2) 
        
        Check if piece1@position1 is attacking the piece2@position2 and vice versa


