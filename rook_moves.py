def rook_moves(pos: str) -> list[str]:
    x, y = pos[0].lower(), pos[1]
    x_range = 'abcdefgh'
    y_range = '12345678'
    
    return [letter + y for letter in x_range if letter != x] + [x + number for number in y_range if number != y]

