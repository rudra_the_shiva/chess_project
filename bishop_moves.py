def bishop_moves(pos: str) -> list[str]:
    x, y = pos[0].lower(), int(pos[1])
    letters = 'abcdefgh'
    top_right = list(zip(letters[letters.index(x) + 1:], list(range(y + 1, 9))))
    bottom_left = list(zip(reversed(letters[:letters.index(x)]), reversed(list(range(1, y)))))
    bottom_right = list(zip(letters[letters.index(x) + 1:], list(range(y - 1, 0, -1))))
    top_left = list(zip(reversed(letters[:letters.index(x)]), list(range(y + 1, 9))))

    return [''.join(pos) for pos in sorted(bottom_left + top_left + bottom_right + top_right)]

