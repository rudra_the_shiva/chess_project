# Contributors:


1. Tanya Tripathi: Function 1: rook\_moves(position)
                   Function 2: queen\_moves(position)


2. Riddhi Jaiswal: Function 1: knight\_moves(position)
                   Function 2: check\_attack(position)

3. Annapurna Adhikari: Function 1: bishop\_moves(position)
