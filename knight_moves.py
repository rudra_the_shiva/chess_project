def knight_attacking_squares(pos: str) ->  list[str]:
    ranks = 'abcdefgh'
    moves = [(2, 1), (2, -1), (-2, 1), (-2, -1),
             (1, 2), (1, -2), (-1, 2), (-1, -2)]
    rank, file = pos[0], int(pos[1])
    x = ranks.index(rank)
    y = file - 1
    possible_moves = []
    for rank_jump, file_jump in moves:
        new_x, new_y = x + rank_jump, y + file_jump
        if 0 <= new_x < 8 and 0 <= new_y < 8:
            possible_moves.append(ranks[new_x] + str(new_y + 1)) 
    return possible_moves
